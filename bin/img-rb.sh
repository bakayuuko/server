#!/bin/env bash
#set root dir
ROOT=/srv/arch
#set current working dir
scdir=$(pwd)

truncate -s 2G /srv/arch.img
mkfs.btrfs /srv/arch.img 2>/dev/null
mkdir -p "$ROOT"
mount -o loop,discard,compress=lzo /srv/arch.img "$ROOT"

pacstrap -c -d "$ROOT" base mkinitcpio-nfs-utils nfs-utils 2>/dev/null
pacman --noconfirm --root "$ROOT" --dbpath "$ROOT/var/lib/pacman" -S grub 2>/dev/null
pacman --noconfirm --root "$ROOT" --dbpath "$ROOT/var/lib/pacman" -S xorg-server xorg-server-utils xorg-xinit ruby ruby-irb 2>/dev/null

sed 's/nfsmount/mount.nfs4/' "$ROOT/usr/lib/initcpio/hooks/net" > "$ROOT/usr/lib/initcpio/hooks/net_nfs4"
cp $ROOT/usr/lib/initcpio/install/net{,_nfs4}
cp "$scdir/ta-mkinitcpio.conf" "$ROOT/etc/ta-mkinitcpio.conf"
arch-chroot "$ROOT" /usr/bin/mkinitcpio -k /boot/vmlinuz-linux -c /etc/ta-mkinitcpio.conf -g /boot/initramfs-linux.img 2>/dev/null
arch-chroot "$ROOT" grub-mknetdir --net-directory=/boot --subdir=grub 2>/dev/null
cp "$scdir/grub.cfg" "$ROOT/boot/grub/grub.cfg"
echo "client" > "$ROOT/etc/hostname"
cp "$scdir/hosts" "$ROOT/etc/hosts"
