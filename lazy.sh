#!/bin/env bash
#set root dir
ROOT=/srv/arch
#set current working dir
scdir=$(pwd)
#set date
now=`date +"%Y-%m-%d[%H:%M:%S]"`
mkdir -p $ROOT

config_menu() {
    echo "backup dhcpd.conf"
    sudo mv /etc/dhcpd.conf "/etc/dhcpd.conf.${now}"
    echo "backup tftpd.service"
    sudo mv /usr/lib/systemd/system/tftpd.service "/usr/lib/systemd/system/tftpd.service.${now}"
    echo "backup exports"
    sudo mv /etc/exports "/etc/exports.${now}"
    echo "Configuring DHCP, TFTP, NFS server"
    cp -fv "${scdir}/dhcp/dhcpd.conf" /etc/dhcpd.conf 2>/dev/null
    cp -fv "${scdir}/tftp/tftpd.service" /usr/lib/systemd/system/tftpd.service 2>/dev/null
    cp -fv "${scdir}/nfs/exports" /etc/exports 2>/dev/null
}

mk_client_menu() {
    while [ answer != "0" ] ; do
        clear
        echo "Select a number:"
        echo "  1) Create client (w/ python)"
        echo "  2) Create client (w/ ruby)"
        echo "  3) Create client (w/ golang)"
        echo "  0) Exit to main menu"
        read -p "Your choice: " answer
        case $answer in
            0) break ;;
            1) sh ${scdir}/bin/img-py.sh ;;
            2) sh ${scdir}/bin/img-rb.sh ;;
            3) sh ${scdir}/bin/img-go.sh ;;
            *) break ;;
        esac
    done
}

net_menu() {
    while [ answer != "0" ] ; do
        clear
        echo "Select a topic:"
        echo "  1) New Networking commands"
        echo "  2) Virtual NICs"
        echo "  3) Vanity Naming"
        echo "  4) IPMP"
        echo "  5) Bandwidth control"
        echo "  0) Exit to main menu"
        read -p "Your choice: " answer
        case $answer in
            0) break ;;
            1) ./bin/net-basics.pl ;;
            2) ./bin/net-vnic.pl ;;
            3) ./bin/net-vanity.pl ;;
            4) ./bin/net-ipmp.pl ;;
            5) ./bin/net-resource.pl ;;
            *) break ;;
        esac
    done
}

ips_menu() {
    while [ answer != "0" ] ; do
        clear
        echo "Select a topic:"
        echo "  1) The basics of IPS"
        echo "  2) Installing and uninstalling packages"
        echo "  3) Checking the integrity of packages"
	echo "  4) Performing a system update"
        echo "  0) Exit to main menu"
        read -p "Your choice: " answer
        case $answer in
            0) break ;;
            1) ./bin/ips-basics.pl ;;
            2) ./bin/ips-install.pl ;;
            3) ./bin/ips-integrity.pl ;;
            4) ./bin/ips-update.pl ;;
            *) break ;;
        esac
    done
}

zones_menu() {
    while [ answer != "0" ] ; do
        clear
        echo "Select a topic:"
        echo "  1) Zone installation"
        echo "  2) Inside the zone"
        echo "  3) Zone cloning"
        echo "  4) Zone monitoring"
        echo "  5) Zone resource management"
        echo "  6) Network resource management"
        echo "  0) Exit to main menu"
        read -p "Your choice: " answer
        case $answer in
            0) break ;;
            1) ./bin/zone-install.pl ;;
            2) ./bin/zone-inside.pl ;;
            3) ./bin/zone-clone.pl ;;
            4) ./bin/zone-monitor.pl ;;
            5) ./bin/zone-resource.pl ;;
            6) ./bin/net-resource.pl ;;
            *) break ;;
        esac
    done
}

dtrace_menu() {
    while [ answer != "0" ] ; do
        clear
        echo "Select a topic:"
        echo "  1) DTrace One-liners"
        echo "  2) DTrace CPU"
        echo "  3) DTrace Disk"
        echo "  0) Exit to main menu"
        read -p "Your choice: " answer
        case $answer in
            0) break ;;
            1) ./bin/dtrace-oneliners.pl ;;
            2) ./bin/dtrace-cpu.pl ;;
            3) ./bin/dtrace-disk.pl ;;
            *) break ;;
        esac
    done
}

sec_menu() {
    while [ answer != "0" ] ; do
        clear
        echo "Select a topic:"
        echo "  1) Roles and privileges"
        echo "  0) Exit to main menu"
        read -p "Your choice: " answer
        case $answer in
            0) break ;;
            1) ./bin/security-roles.pl ;;
            *) break ;;
        esac
    done
}

smf_menu() {
    while [ answer != "0" ] ; do
        clear
        echo "Select a topic:"
        echo "  1) SMF Basics"
        echo "  2) Enabling and disabling services"
        echo "  0) Exit to main menu"
        read -p "Your choice: " answer
        case $answer in
            0) break ;;
            1) ./bin/smf-basics.pl ;;
            2) ./bin/smf-enable.pl ;;
            *) break ;;
        esac
    done
}

# Main Menu
while [ answer != "0" ]  
do 
clear 
echo "Select a topic:" 
echo "  1) Image Packaging System (IPS)" 
echo "  2) Boot Environments" 
echo "  3) ZFS" 
echo "  4) Zones" 
echo "  5) Service Management Facility (SMF)"
echo "  6) Networking" 
echo "  7) DTrace" 
echo "  8) Security" 
echo "  0) EXIT" 
read -p "Your choice: " answer 
    case $answer in 
	0) break ;; 
	1) ips_menu ;;
	2) be_menu ;;
	3) zfs_menu ;; 
	4) zones_menu ;;
	5) smf_menu ;;
	6) net_menu ;;
	7) dtrace_menu ;;
	8) sec_menu ;;
	*) break ;; 
   esac  
done 
exit 0
